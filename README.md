
| Docker Image        |      Tag                           |
|---------------------|:----------------------------------:|
| schleyk/nextcloud   | 22, 23, 24                         |
| schleyk/nginx-php   | 7.4, 8.0, 8.1                      |
| schleyk/apache-php  | 7.4, 8.0, 8.1                      |
| schleyk/guacamole   | 1.4.0                              |
